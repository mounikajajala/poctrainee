import csv
import json

def convert_json(l):
    '''converting the file in list into json format '''
    json_format = []
    for row in l:
        dict_row = {
            "SN": row[0],
            "Name": row[1],
            "City": row[2]
        }
        json_format.append(dict_row)
    data = json.dumps(json_format)
    print(data)

with open("csvfile1.csv", 'r') as csvfile:
    '''opening and reading local file and then appending to list '''
    read = csv.reader(csvfile, skipinitialspace=True)
    l = []
    for row in read:
        l.append(row)

convert_json(l)
